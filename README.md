# Blockchain

Simple blockchain implementation for educational purposes at Mathecamp 2022.

## License

Code is licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt), see [LICENSE](LICENSE).
